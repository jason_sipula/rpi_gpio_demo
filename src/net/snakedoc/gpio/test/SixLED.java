package net.snakedoc.gpio.test;

import framboos.OutPin;

public class SixLED {
    
    public static void main(String[] args) throws InterruptedException {
        
        
        OutPin leds[] = new OutPin[6];
        
        leds[0] = new OutPin(0);
        leds[1] = new OutPin(1);
        leds[2] = new OutPin(2);
        leds[3] = new OutPin(3);
        leds[4] = new OutPin(4);
        leds[5] = new OutPin(5);
        
        try {
        
            for (OutPin led : leds) {
            
                led.setValue(true);
            
            }
        
            Thread.sleep(5000);
        
            for (OutPin led : leds) {
            
                led.setValue(false);
            
            }
        
            Thread.sleep(800);
        
            for (OutPin led : leds) {
            
                led.setValue(true);
                Thread.sleep(800);
            
            }
        
            Thread.sleep(2000);
        
            for (int i = 0; i < 5; i++) {
            
                for (OutPin led : leds) {
            
                    led.setValue(false);
                    Thread.sleep(250);
                    led.setValue(true);
            
                }  
            
            }
            
            for (int i = 0; i < 5; i++) {
                
                for (OutPin led : leds) {
            
                    led.setValue(false);
                    Thread.sleep(125);
                    led.setValue(true);
            
                }  
            
            }
            
            for (int i = 0; i < 5; i++) {
                
                for (OutPin led : leds) {
            
                    led.setValue(false);
                    Thread.sleep(50);
                    led.setValue(true);
            
                }  
            
            }
        
            for (OutPin led : leds) {
            
                led.setValue(false);
            
            }
        
            Thread.sleep(2000);
        
            for (int i = 0; i < 3; i++) {
                for (int k = (leds.length - 1); k > -1; k--) {
            
                    leds[k].setValue(true);
                    Thread.sleep(250);
                    leds[k].setValue(false);
            
                }
            
            }
            
            for (OutPin led : leds) {
            
                led.setValue(false);
            
            }
        
            for (OutPin led : leds) {
            
                led.close();
            
            }
        } catch (Exception e) {
            
            e.printStackTrace();
            for (OutPin led : leds) {
                
                if (led != null) {
                    if (led.getValue() != false) {
                        led.setValue(false);
                    }
                    led.close();
                }
            }
            
        }
    }
    
}
