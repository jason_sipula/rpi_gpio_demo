package net.snakedoc.gpio.test;

import java.util.Scanner;

import framboos.OutPin;

public class KeyedLED {

    public static void main(String[] args) {
        
        OutPin leds[] = new OutPin[6];
        
        leds[0] = new OutPin(0);
        leds[1] = new OutPin(1);
        leds[2] = new OutPin(2);
        leds[3] = new OutPin(3);
        leds[4] = new OutPin(4);
        leds[5] = new OutPin(5);
        
        try {
        
            System.out.println("\nEntering Key Listener Mode!\n");

            for (OutPin led : leds) {
            
                led.setValue(false);
        
            }
        
            Scanner in = new Scanner(System.in);
            int pin = -1;
            while ((pin = in.nextInt()) != -1) {
                
                if (pin <= 5 && pin >= 0) {
                    if (leds[pin].getValue() == false) {
                        leds[pin].setValue(true);
                    } else {
                        leds[pin].setValue(false);
                    }
                }
            }
            in.close();
            
            for (OutPin led : leds) {
                
                led.setValue(false);
            
            }
        
            for (OutPin led : leds) {
            
                led.close();
            
            }
            
        } catch (Exception e) {
            
            e.printStackTrace();
            for (OutPin led : leds) {
                
                if (led != null) {
                    if (led.getValue() != false) {
                        led.setValue(false);
                    }
                    led.close();
                }
            }    
        }
    }
}
