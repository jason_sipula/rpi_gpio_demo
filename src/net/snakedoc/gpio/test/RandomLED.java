package net.snakedoc.gpio.test;

import java.security.SecureRandom;

import framboos.OutPin;

public class RandomLED {
    
    private static OutPin leds[] = new OutPin[6];
    
    public static void main(String[] args) {
        
        JVMShutDownHook shutdownHook = new JVMShutDownHook();
        Runtime.getRuntime().addShutdownHook(shutdownHook);
    
        leds[0] = new OutPin(0);
        leds[1] = new OutPin(1);
        leds[2] = new OutPin(2);
        leds[3] = new OutPin(3);
        leds[4] = new OutPin(4);
        leds[5] = new OutPin(5);
        
        SecureRandom random = new SecureRandom();
    
        try {
            
            int i = 1;
            while (i == 1) {
                
                int curPin = random.nextInt(6);
                if (leds[curPin].getValue() == false) {
                    leds[curPin].setValue(true);
                } else {
                    leds[curPin].setValue(false);
                }
                Thread.sleep(random.nextInt(301));
                
            }
            
        } catch (Exception e) {
    
            e.printStackTrace();
            for (OutPin led : leds) {
        
                if (led != null) {
                    if (led.getValue() != false) {
                        led.setValue(false);
                    }
                    led.close();
                }
            }
        }
    }
    
    
    private static class JVMShutDownHook extends Thread {
        
        public void run() {
            for (OutPin led : leds) {
                
                led.setValue(false);
    
            }

            for (OutPin led : leds) {
    
                led.close();
    
            }
        }
    }
}
